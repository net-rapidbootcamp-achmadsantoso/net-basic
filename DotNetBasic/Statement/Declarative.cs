﻿public class Declarative
{
    public static void SampleDeclarative()
    {
        Console.WriteLine("Declarative Test");

        //variable declaration statement
        double area = 1.5;
        Console.WriteLine("area value = " + area);
        double radius = 2;
        Console.WriteLine("radius value = " + radius);

        //constant declaration statement
        const double pi = 3.14159;
        Console.WriteLine("Pi value = " + pi);
    }
}