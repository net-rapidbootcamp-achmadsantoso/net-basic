﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statement
{
    public class MainSatement
    {
        public MainSatement()
        {
        }

        public static void Main()
        {
            Declarative.SampleDeclarative();
            ForStatement.SampleFor();
        }
    }
}
