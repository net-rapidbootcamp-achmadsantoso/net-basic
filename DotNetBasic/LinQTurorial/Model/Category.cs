﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQTurorial.Model
{
   public class Category
    {
        public string CategoryName { get; set; }
        public int Count { get; set; }

        public Category(string name, int count)
        {
            this.CategoryName = name;
            this.Count= count;
        }
    }
}
