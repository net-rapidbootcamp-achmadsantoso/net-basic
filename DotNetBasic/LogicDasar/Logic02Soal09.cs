﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDasar
{
    public class Logic02Soal09
    {

        public static void IsiData(int n)
        {
            string[,] array = new string[n,n];
            int[] angka = new int[n];
            
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (j - i <= n/2 && i - j <= n/2 &&
                i + j >= n/2 && i + j <= n/2 + n - 1
)
                    {
                        array[i, j] = Convert.ToString(angka[j]);
                    }
                }
            }
            Print(array);
        }

        public static void Print(string[,] array)
        {
            for(int i = 0; i< array.GetLength(0); i++)
            {
                for(int j = 0; j< array.GetLength(1); j++)
                {
                    if (array[i,j] != null)
                    {
                        Console.Write(array[i,j] + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine("\n");
            }
        }

    }
}
