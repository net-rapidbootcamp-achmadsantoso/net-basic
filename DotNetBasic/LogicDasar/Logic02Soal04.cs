﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicDasar
{
    public class Logic02Soal04
    {

        public static void IsiData(int n)
        {
            string[,] array = new string[n,n];
            int[] angka = new int[n];
            
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (j <= 1)
                    {
                        angka[j] = 1;
                    }
                    else
                    {
                        angka[j] = angka[j-1] + angka[j - 2];
                    }
                    if (i == 0 || j == 0 || i == n - 1 || j == n - 1 || i == n/2 || j == n/2)
                    {
                        array[i, j] = Convert.ToString(angka[j]);
                    }
                }
            }
            Print(array);
        }

        public static void Print(string[,] array)
        {
            for(int i = 0; i< array.GetLength(0); i++)
            {
                for(int j = 0; j< array.GetLength(1); j++)
                {
                    if (array[i,j] != null)
                    {
                        Console.Write(array[i,j] + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine("\n");
            }
        }

    }
}
